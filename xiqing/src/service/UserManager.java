package service;

public interface UserManager {
	//for Test
	public boolean checkUser(String userName,String passwd);
	
	//create a user with phone and password
	public int registUser(String userName,String phoneNum,String passwd);
	
	//complete the new user's infomation
	public boolean modifyPasswd(String phoneNum,String passwd);

}
