package service;

import dao.IUserDao;
import dao.UserDao;
import utils.Constants;
import po.*;

public class IUserManager implements UserManager{
	private static UserDao ud;
	private static boolean flag;
	private static Constants cons;
	
	public IUserManager() {
		ud=new IUserDao();
		flag=false;
		cons=new Constants();
	}
	@Override
	public boolean checkUser(String userName, String passwd) {
		User user=ud.getUserByName(userName);
		if(null!=user){
			String user_passwd=user.getUpassword();
			if(passwd.equals(user_passwd)){
				return true;
			}			
		}
		return false;
	}
	
	@Override
	public int registUser(String userName, String phoneNum, String passwd) {
		boolean flagName;
		boolean flagCreate;
		flag=ud.checkPhoneNum(phoneNum);
		flagName=ud.checkUserName(userName);
		//phoneNum not registered
		if(!flag){
			//userName not registered
			if(!flagName){
				User user= new User(userName,phoneNum,passwd,cons.USER_COMMEN);
				flagCreate=ud.createUser(user);
				if(flagCreate){
					return cons.REGIST_SUCCESS;
				}else{
					return cons.REGIST_CREATE_FAIL;
				}	
			}else
				return cons.REGIST_USERNAME_WRONG;
		}else{
			if(!flagName)
				return cons.REGIST_PHONE_WRONG;
			else
				return cons.REGIST_PHONEUSER_WRONG;
		}
	}

	@Override
	public boolean modifyPasswd(String phoneNum, String passwd) {
		User user=ud.getUserByPhoneNum(phoneNum);
		if(null!=user){
			user.setUpassword(passwd);
			flag=ud.updateUserByPhoneNum(phoneNum, user);
			if(flag)
				return true;
		}
		return false;
		
	}
	
}
