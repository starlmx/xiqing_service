package po;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_user")
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer uid;	
	@Column(unique = true,nullable=false)
	private String uname;
	@Column(nullable=false)
	private String upassword;
	@Column(unique = true,nullable=false)
	private String uphoneNum;
	private int authority;
	
	
	public int getAuthority() {
		return authority;
	}

	public void setAuthority(int authority) {
		this.authority = authority;
	}

	public User() {
		super();
	}

	public User(String uname, String upassword, String uphoneNum, int authority) {
		super();
		this.uname = uname;
		this.upassword = upassword;
		this.uphoneNum = uphoneNum;
		this.authority = authority;
	}

	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getUpassword() {
		return upassword;
	}
	public void setUpassword(String upassword) {
		this.upassword = upassword;
	}
	public String getUphoneNum() {
		return uphoneNum;
	}
	public void setUphoneNum(String uphoneNum) {
		this.uphoneNum = uphoneNum;
	}

	@Override
	public String toString() {
		return "User [uid=" + uid + ", uname=" + uname + ", upassword=" + upassword + ", uphoneNum=" + uphoneNum
				+ ", authority=" + authority + "]";
	}
	

}
