package utils;

public class Constants {
	public final int REGIST_SUCCESS=0;
	public final int REGIST_PHONE_WRONG=1;
	public final int REGIST_USERNAME_WRONG=2;
	public final int REGIST_PHONEUSER_WRONG=3;
	public final int REGIST_CREATE_FAIL=4;
	
	public final int USER_ADMINISTER=4;
	public final int USER_FIXER=3;
	public final int USER_CHEF=2;
	public final int USER_COMMEN=1;
	
}
