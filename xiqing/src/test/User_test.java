package test;

import org.junit.Test;

import dao.IUserDao;
import po.User;

public class User_test {
	
	@Test //初始化数据库
	public void test() {	
		IUserDao iud=new  IUserDao();
		User user = new User("fanchao","123456","18602990001",1);
		iud.createUser(user);
		User user2 = new User("fanchao2","123456","18602990002",1);
		iud.createUser(user2);
		User user3 = new User("wangshuang","123456","18602990003",1);
		iud.createUser(user3);
		User user4 = new User("wangshuang2","123456","18602990004",1);
		iud.createUser(user4);
	}
	@Test
	public void test1_getUserByName(){		
		User user=new IUserDao().getUserByName("fanchao");
		System.out.println(user);
	}
	@Test
	public void test2_getUserByPhoneNum(){		
		User user=new IUserDao().getUserByPhoneNum("18602990003");
		System.out.println(user);
	}
	
	@Test
	public void test3_checkPhoneNum(){		
		boolean b=new IUserDao().checkPhoneNum("18602990003");
		System.out.println(b);
	}
	
	@Test
	public void test4_checkUserName() {
		String uName="wangshuang";
		boolean f=new IUserDao().checkUserName(uName);
		System.out.println(f);

	}
	@Test
	public void test5_createUser(){
		User user = new User("ws1","123456","187236434",1);
		boolean f = new  IUserDao().createUser(user);
		System.out.println(f);
	}
	
	@Test
	public void test6_updateUserByName(){
		User user = new User("ws1","w123456","187236434",1);
		String name=user.getUname();
		boolean f = new IUserDao().updateUserByName(name, user);
		System.out.println(f);
	}
	
	@Test
	public void test7_updateUserByPhoneNum(){
		User user = new User("w1s","123www","187236434",1);
		String phoneNum = user.getUphoneNum();
		boolean f = new IUserDao().updateUserByPhoneNum(phoneNum, user);
		System.out.println(f);
	}
	@Test
	public void test8_deleteUserByName() {
		String name="w1s";
		boolean f=new IUserDao().deleteUserByName(name);
		System.out.println(f);

	}
	@Test
	public void test9_deleteUserByPhoneNum() {
		String phoneNum="187236434";
		boolean f=new IUserDao().deleteUserByPhoneNum(phoneNum);
		System.out.println(f);
		
	}

}
