package controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import service.IUserManager;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.IUserManager;
@Controller
@RequestMapping("/user")
public class UserController {
	@RequestMapping(value = "/login/{name}/{passwd}", method = RequestMethod.GET)
	@ResponseBody
	public Object login(@PathVariable("name") String uname, @PathVariable("passwd") String passwd,
			HttpServletResponse response) throws IOException {
		IUserManager iuser=new IUserManager();
		if(iuser.checkUser(uname, passwd)){
			response.addHeader("flag", "true");
		}else{
			response.addHeader("flag", "false");
		}
		return "login";
	}
	
	//注册
	@RequestMapping(value = "/regist", method = RequestMethod.POST)
	@ResponseBody
	public Object regist(@RequestParam("uname") String uname,@RequestParam("phonenum") String phonenum, @RequestParam("passwd") String passwd,HttpServletResponse response) {
		IUserManager iuser=new IUserManager();
		//注册成功：0
		//用户已存在：1
		//用户名已注册：2
		//用户已存在且用户名已注册：3
		//失败：4
		if(iuser.registUser(uname,phonenum,passwd)==0){
			response.addHeader("flag", "success");
		}else if(iuser.registUser(uname,phonenum,passwd)==1){
			response.addHeader("flag", "userexist");
		}else if(iuser.registUser(uname,phonenum,passwd)==2){
			response.addHeader("flag", "nameexist");
		}else if(iuser.registUser(uname,phonenum,passwd)==3){
			response.addHeader("flag", "bothexist");
		}else if(iuser.registUser(uname,phonenum,passwd)==4){
			response.addHeader("flag", "fail");
		}
		return "regist";
	}
	
	//修改密码
	@RequestMapping(value = "/modifypasswd", method = RequestMethod.POST)
	@ResponseBody
	public Object modifyPassword(@RequestParam("phonenum") String phonenum, @RequestParam("passwd") String passwd,HttpServletResponse response) {
		IUserManager iuser=new IUserManager();
		if(iuser.modifyPasswd(phonenum,passwd)){
			response.addHeader("flag", "true");
		}else{
			response.addHeader("flag", "false");
		}
		return "modify password";
	}
}
