package dao;

import org.hibernate.Session;
import po.User;
import utils.HbnUtil;

public class IUserDao implements UserDao{
	@Override
	public User getUserByName(String name) {
		Session session=HbnUtil.getSession();
		User user=null;
		try {
			session.beginTransaction();
			String hql=" from User where uname=:name";
			user=(User) session.createQuery(hql).setParameter("name", name).uniqueResult();
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		return user;
	}

	@Override
	public User getUserByPhoneNum(String phoneNum) {
		Session session=HbnUtil.getSession();
		User user=null;
		try {
			session.beginTransaction();
			String hql=" from User where uphoneNum=:phoneNum";
			user=(User) session.createQuery(hql).setParameter("phoneNum", phoneNum).uniqueResult();
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		return user;
	}

	@Override
	public boolean checkPhoneNum(String phoneNum) {
		Session session=HbnUtil.getSession();
		long count=0;
		try {
			session.beginTransaction();
			String hql=" select count(*)from User where uphoneNum=:phoneNum";
			count=(long)session.createQuery(hql).setParameter("phoneNum", phoneNum).uniqueResult();
			session.getTransaction().commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		return count>0;
	}

	@Override
	public boolean checkUserName(String userName) {
		Session session=HbnUtil.getSession();
		long count = 0;
		try {
			session.beginTransaction();
			String hql=" select count(*) from User where uname=:name";
			count = (long) session.createQuery(hql).setParameter("name", userName).uniqueResult();			
			session.getTransaction().commit();			
			} catch (Exception e) {
			e.printStackTrace();				
			session.getTransaction().rollback();
		}
		return count > 0;
	}

	@Override
	public boolean createUser(User user) {
		Session session=HbnUtil.getSession();
		try {
			session.beginTransaction();		
			session.saveOrUpdate(user);
			session.getTransaction().commit();			
			} catch (Exception e) {
			e.printStackTrace();				
			session.getTransaction().rollback();
			return false;
		}
		return true;
	}

	@Override
	public boolean updateUserByName(String oldName, User user) {
		User oldUser = new IUserDao().getUserByName(oldName);
		Integer uid = oldUser.getUid();
		user.setUid(uid);
		Session session=HbnUtil.getSession();
		try {
			session.beginTransaction();			
			session.update(user);
			session.getTransaction().commit();			
			} catch (Exception e) {
			e.printStackTrace();				
			session.getTransaction().rollback();
			return false;
		}
		return true;
	}

	@Override
	public boolean updateUserByPhoneNum(String oldPhoneNum, User user) {
		User oldUser = new IUserDao().getUserByPhoneNum(oldPhoneNum);
		Integer uid = oldUser.getUid();
		user.setUid(uid);
		Session session=HbnUtil.getSession();
		try {
			session.beginTransaction();			
			session.update(user);
			session.getTransaction().commit();			
			} catch (Exception e) {
			e.printStackTrace();				
			session.getTransaction().rollback();
			return false;
		}
		return true;
	}

	@Override
	public boolean deleteUserByName(String name) {
		User user=new IUserDao().getUserByName(name);
		Session session=HbnUtil.getSession();
		try {
			session.beginTransaction();
			session.delete(user);
			session.getTransaction().commit();			
			} catch (Exception e) {
			e.printStackTrace();				
			session.getTransaction().rollback();
			return false;
		}
		return true;
		
	}

	@Override
	public boolean deleteUserByPhoneNum(String phoneNum) {
		User user=new IUserDao().getUserByPhoneNum(phoneNum);
		Session session=HbnUtil.getSession();
		try {
			session.beginTransaction();
			session.delete(user);
			session.getTransaction().commit();			
			} catch (Exception e) {
			e.printStackTrace();				
			session.getTransaction().rollback();
			return false;
		}
		return true;
	}

	

	

}
