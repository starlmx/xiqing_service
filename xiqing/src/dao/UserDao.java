package dao;

import po.User;

public interface UserDao {
	//get a user by userName
	public User getUserByName(String name);
	
	//get a user by userPhoneNumber;
	public User getUserByPhoneNum(String phoneNum);
	
	//check if the phoneNum exists
	public boolean checkPhoneNum(String phoneNum);
	
	//check if the userName exists
	public boolean checkUserName(String userName);

	//create a user 
	public boolean createUser(User user);
	
	//update a user by Name
	public boolean updateUserByName(String oldName,User user);
		
	//update a user by phoneNum
	public boolean updateUserByPhoneNum(String oldPhoneNum,User user);
	
	//delete a user by name
	public boolean deleteUserByName(String name);
	
	//delete a user by phoneNum
	public boolean deleteUserByPhoneNum(String phoneNum);
		
}
